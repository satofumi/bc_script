import sys
sys.path.append('../lib')

import packet
import command


def title_test(command, result_wait):
    if not command.wait_scene_changed('Title'):
        print('scene error: ' + __name__)
        return

    command.send('Title.Version')
    command.send('KeyCode.Escape')

    command.send('Title.Ranking')
    command.send('Title.Back')

    command.send('Title.WatchReplay')
    command.send('Title.Back')

    command.send('Title.MapEditor')
    command.wait_scene_changed('MapEditor')
    command.send('MapEditor.Back')
    command.wait_scene_changed('Title')

    command.send('Title.ChangeAppearance')
    command.wait_scene_changed('ChangeAppearance')
    command.send('ChangeAppearance.Back')
    command.wait_scene_changed('Title')

    command.send('Title.Option')
    command.send('Title.Back')

    command.send('Title.Help')
    command.send('Title.Back')

    command.send('Title.PlayGame')
    command.send('Title.Back')

    command.send('Title.Credit')
    command.wait_scene_changed('Credit')
    command.send('Credit.Back')
    command.wait_scene_changed('Title')

    command.send('Title.PlayGame')
    command.send('Title.Tutorial')
    command.wait_scene_changed('Campaign')
    command.send('Campaign.Back')
    command.wait_scene_changed('Title')
    command.send('Title.Back')


if __name__ == '__main__':
    time_ratio = 1.0
    packet = packet.Packet()
    packet.connect()
    command = command.Command(packet)
    result_wait = 1.0
    title_test(command, result_wait)
    packet.close()
