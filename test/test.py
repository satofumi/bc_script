import sys
sys.path.append('../lib')

import packet
import command
import time


if __name__ == '__main__':
    time_ratio = 1.0
    packet = packet.Packet()
    packet.connect()
    command = command.Command(packet)

    command.wait_enable_and_press('Campaign.Tutorial.Title.stage_1')
    command.wait_enable_and_press('Campaign.Start')

    packet.close()
