import sys
sys.path.append('../lib')

import packet
import command
import time


def play_plorogue(command, result_wait):
    command.wait_enable_and_press('Campaign.Campaign\\00000000.Title.prologue')
    time.sleep(result_wait)
    command.send('KeyCode.Escape')
    time.sleep(1.0)
    command.wait_scene_changed('Campaign')


def play_stage_1(command, result_wait):
    command.wait_enable_and_press('Campaign.Campaign\\00000000.Title.training_area')
    #command.wait_enable_and_press('Campaign.Start')

    # skip first messages
    command.wait_scene_changed('Game')
    command.send('Game.ConfirmPanelOk')
    command.send('KeyCode.Escape')

    command.skip_before_build()

    # toggle bird view
    command.send('KeyCode.Return')

    # build phase
    command.place_turret('BowTurret', 12, 19)
    command.place_turret('BowTurret', 12, 18)

    # defense phase
    command.enter_defense()

    ## speed up
    command.send('Game.Play')
    command.send('Game.Play')

    # wait defense phase end
    command.wait_defense_end()

    # gather phase
    command.enter_gather()
    command.wait_gather_end()

    # round 2
    command.skip_before_build()
    command.place_turret('BowTurret', 12, 16)
    command.skip_defense_and_gather()

    # round 3
    command.skip_before_build()
    command.place_turret('BowTurret', 12, 15)
    command.enter_defense()

    command.wait_enable_and_press('Game.BackToCampaign')
    command.wait_scene_changed('Campaign')


def play_stage_2(command, result_wait):
    command.wait_enable_and_press('Campaign.Campaign\\00000000.Title.first_toride')
    #command.wait_enable_and_press('Campaign.Start')

    # skip first messages.
    command.wait_scene_changed('Game')
    command.send('Game.ConfirmPanelOk')

    command.skip_before_build()

    # start research
    command.research_tech('Tech_Enemy_0')
    command.place_turret('BowTurret', 18, 20)
    command.enter_defense()
    command.send('Game.Play')
    command.send('Game.Play')
    command.wait_defense_end()
    command.enter_gather()
    command.wait_gather_end()

    # round 2
    command.skip_before_build()
    command.research_tech('Tech_Enemy_1')
    command.place_turret('BowTurret', 14, 21)
    command.place_turret('BowTurret', 16, 18)
    command.skip_defense_and_gather()

    # round 3
    command.skip_before_build()
    command.skip_defense_and_gather()

    # round 4
    command.skip_before_build()
    command.research_tech('Tech_Weaken_0')
    command.skip_defense_and_gather()

    # round 5
    command.skip_before_build()
    command.skip_defense_and_gather()

    # round 6
    command.skip_before_build()
    command.research_tech('Tech_Weaken_1')
    command.skip_defense_and_gather()

    # round 7
    command.skip_before_build()
    command.wait_enable_and_press('Game.BackToCampaign')
    command.wait_scene_changed('Campaign')


def play_epilogue(command, result_wait):
    command.send('Campaign.Campaign\\00000000.Title.epilogue', result_wait)
    command.send('KeyCode.Escape', result_wait)


def tutorial_test(command, result_wait):
    scene = command.scene()
    if scene == 'Title':
        command.send('Title.PlayGame')
        command.send('Title.NewGame')
        command.send('Title.Campaign')

    if not command.wait_scene_changed('Campaign'):
        print('scene error: ' + __name__)
        return

    # prologue
    #play_plorogue(command, result_wait)

    # stage_1
    play_stage_1(command, result_wait)

    # stage_2
    play_stage_2(command, result_wait)

    # epilogue
    #play_epilogue(command, result_wait)

    command.wait_enable_and_press('Campaign.Back')
    command.wait_enable_and_press('Title.Back')


if __name__ == '__main__':
    packet = packet.Packet()
    packet.connect()
    command = command.Command(packet)
    result_wait = 1.0
    tutorial_test(command, result_wait)
    packet.close()
