import sys
sys.path.append('../lib')

import packet
import command
import check


def play_defeated_test(command, result_wait):
    scene_name = command.scene()
    if scene_name == 'Title':
        command.send('Title.PlayGame')
        command.send('Title.NewGame')
        command.send('Title.NormalMode')
        command.wait_scene_changed('MapSelect')

    if not command.wait_scene_changed('MapSelect'):
        print('scene_error: ' + __name__)
        return

    command.wait_enable('MapSelect.InputField.MapCode')
    command.send('MapSelect.InputField.MapCode.4F665619')
    command.wait_enable_and_press('MapSelect.Start')

    # skip first messages
    command.wait_scene_changed('Game')
    command.send('Game.ConfirmPanelOk')
    command.send('KeyCode.Escape')

    # check resource values
    command.skip_before_build()
    check.values(command.rounds(), 1)
    check.values(command.population(), 5)
    check.values(command.build_point(), 50)
    check.values(command.spirit_point(), 30)
    check.values(command.resources(), 0)

    # build phase
    command.build_building('Hut', 39, 47)
    command.send('KeyCode.Escape')

    # defense phase
    command.enter_defense()

    ## speed up
    command.send('Game.Play')
    command.send('Game.Play')

    # wait defense phase end
    command.wait_defeat_end()

    # back to title
    command.send('KeyCode.Escape')
    command.send('Game.BackToTitle')
    command.send('Game.ConfirmPanelOk')
    command.wait_scene_changed('Title')


if __name__ == '__main__':
    time_ratio = 1.0
    packet = packet.Packet()
    packet.connect()
    command = command.Command(packet)
    result_wait = 1.0
    play_defeated_test(command, result_wait)
    packet.close()
