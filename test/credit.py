import sys
sys.path.append('../lib')

import packet
import command
import time


def credit_test(command, result_wait):
    command.send('Title.Credit')
    if not command.wait_scene_changed('Credit'):
        print('scene error: ' + __name__)
        return

    # !!!


if __name__ == '__main__':
    packet = packet.Packet()
    packet.connect()
    command = command.Command(packet)
    result_wait = 1.0
    credit_test(command, result_wait)
    packet.close()
