import sys
sys.path.append('../lib')

import packet
import command
import title
import normal_game
import tutorial
import credit


def all_test(command, result_wait):
    title.title_test(command, result_wait)
    normal_game.play_one_round_test(command, result_wait)
    tutorial.tutorial_test(command, result_wait)
    command.send('Campaign.Back', result_wait)

    #credit.credit_test(packet, time_ratio)

    command.send('Title.Quit')


if __name__ == '__main__':
    packet = packet.Packet()
    packet.connect()
    command = command.Command(packet)

    result_wait = 1.0
    all_test(command, result_wait)

    packet.close()
