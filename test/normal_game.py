import sys
sys.path.append('../lib')

import packet
import command
import check


def play_one_round_test(command, result_wait):
    scene_name = command.scene()
    if scene_name == 'Title':
        command.send('Title.PlayGame')
        command.send('Title.NewGame')
        command.send('Title.NormalMode')
        command.wait_scene_changed('MapSelect')

    if not command.wait_scene_changed('MapSelect'):
        print('scene_error: ' + __name__)
        return

    command.wait_enable('MapSelect.InputField.MapCode')
    command.send('MapSelect.InputField.MapCode.4F665619')
    command.wait_enable_and_press('MapSelect.Start')

    # skip first messages
    command.wait_scene_changed('Game')
    command.send('Game.ConfirmPanelOk')
    command.send('KeyCode.Escape')

    # check resource values
    command.skip_before_build()
    check.values(command.rounds(), 1)
    check.values(command.population(), 5)
    check.values(command.build_point(), 50)
    check.values(command.spirit_point(), 30)
    check.values(command.resources(), 0)

    # build phase
    command.build_building('House', 39, 47)
    command.place_turret('BowTurret', 37, 46)
    command.place_turret('BowTurret', 42, 49)
    command.place_turret('BowTurret', 42, 46)

    # defense phase
    command.enter_defense()

    ## speed up
    command.send('Game.Play')
    command.send('Game.Play')

    # wait defense phase end
    command.wait_defense_end()

    # gather phase
    command.enter_gather()
    command.wait_gather_end()

    # check resource values
    command.skip_before_build()
    check.values(command.rounds(), 2)
    check.values(command.population(), 9)
    check.values(command.build_point(), 90)
    check.values(command.spirit_point(), 24)
    check.values(command.resources(), 20)

    # dig and fill soil test
    command.dig_soil(50, 50)
    check.values(command.build_point(), 85)
    command.fill_soil(51, 50)
    check.values(command.build_point(), 80)

    command.dig_soil(52, 50)
    check.values(command.build_point(), 75)
    command.fill_soil(52, 50)
    check.values(command.build_point(), 80)
    command.fill_soil(52, 50)
    check.values(command.build_point(), 75)

    command.fill_soil(53, 50)
    check.values(command.build_point(), 70)
    command.dig_soil(53, 50)
    check.values(command.build_point(), 75)
    command.dig_soil(53, 50)
    check.values(command.build_point(), 70)

    # cancel dig_soil selected
    command.send('KeyCode.Escape')


    # back to title
    command.send('KeyCode.Escape')
    command.send('Game.BackToTitle')
    command.send('Game.ConfirmPanelOk')
    command.wait_scene_changed('Title')


if __name__ == '__main__':
    time_ratio = 1.0
    packet = packet.Packet()
    packet.connect()
    command = command.Command(packet)
    result_wait = 1.0
    play_one_round_test(command, result_wait)
    packet.close()
