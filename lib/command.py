import packet
import time


class Command:
    def __init__(self, packet):
        self._packet = packet


    def _print_error(self, message):
        print(message)


    def send(self, message, timeout=5.0):
        self._packet.send(message)
        response = self._packet.recv(timeout)
        if response != '.':
            error_message = "!send: '{0}' response is '{1}'".format(message, response)
            self._print_error(error_message)
            return False

        time.sleep(0.2)
        return True


    def _send_and_recv(self, message, timeout=1.0):
        self._packet.send(message)
        time.sleep(0.2)
        return self._packet.recv(timeout)


    def scene(self, timeout=1.0):
        return self._send_and_recv('scene?', timeout)


    def phase(self, timeout=1.0):
        return self._send_and_recv('phase?', timeout)


    def ui_phase(self, timeout=1.0):
        return self._send_and_recv('ui_phase?', timeout)


    def is_enable(self, button_name, timeout=5.0):
        if self._send_and_recv(button_name + '?', timeout) == 'enable':
            return True
        else:
            return False


    def wait_scene_changed(self, expected_scene):
        timeout = 10.0
        start = time.time()
        while ((time.time() - start) < timeout):
            response = self.scene(timeout)
            if response == expected_scene:
                return True
            time.sleep(0.2)

        error_message = "!wait_scene_changed: '{0}'".format(expected_scene)
        self._print_error(error_message)
        return False


    def build_building(self, name, x, y):
        self.send('Game.FocusGrid.{0}.{1}'.format(x, y))
        self.send('KeyCode.E')
        self.send('Game.Build.{0}'.format(name))
        self.send('Mouse.LeftClick')
        self.send('Game.ReleaseFocus')


    def place_turret(self, name, x, y):
        self.send('Game.FocusGrid.{0}.{1}'.format(x, y))
        self.send('KeyCode.E')
        self.send('Game.Turret.{0}'.format(name))
        self.send('Mouse.LeftClick')
        self.send('Game.ReleaseFocus')

        response = self.ui_phase()
        if response == 'PlacePreview':
            self.send('KeyCode.Escape')


    def dig_soil(self, x, y):
        self.build_building('DigSoil', x, y)


    def fill_soil(self, x, y):
        self.build_building('FillSoil', x, y)


    def skip_before_build(self):
        self.wait_enable_and_press('Game.MessagePanelOk')


    def enter_defense(self):
        self.wait_enable_and_press('Game.NextPhase')
        self.wait_enable_and_press('Game.ConfirmPanelOk')
        self.wait_enable_and_press('Game.MessagePanelOk')


    def wait_defense_end(self):
        self.wait_enable_and_press('Game.MessagePanelOk')


    def wait_defeat_end(self):
        self.wait_enable_and_press('Game.BackToBuildPhase')


    def enter_gather(self):
        self.wait_enable_and_press('Game.MessagePanelOk')


    def wait_gather_end(self):
        self.wait_enable_and_press('Game.MessagePanelOk')


    def skip_defense_and_gather(self):
        self.enter_defense()
        self.wait_defense_end()
        self.enter_gather()
        self.wait_gather_end()


    def wait_enable_and_press(self, button_name, timeout=0):
        self.wait_enable(button_name, timeout)
        self.send(button_name)


    def wait_enable(self, name, timeout=0):
        start_time = time.time()
        while True:
            time.sleep(0.25)
            if self.is_enable(name):
                return
            if timeout > 0:
                elapsed = time.time() - start_time
                if timeout > elapsed:
                    return


    def exit_to_title(self):
        self.send('KeyCode.Escape')
        self.send('Game.BackToTitle')
        self.send('Game.ConfirmPanelOk')
        self.wait_scene_changed('Title')


    def research_tech(self, tech_name):
        self.send('KeyCode.E')
        time.sleep(0.2)
        self.send('KeyCode.Tab')
        time.sleep(0.2)
        self.wait_enable_and_press('Game.Tech.{0}'.format(tech_name), 0.5)

        while True:
            if not self.is_control_panel_active():
                break
            self.send('KeyCode.Escape')

    def rounds(self):
        return int(self._send_and_recv('Value.Rounds?'))

    def max_population(self):
        return int(self._send_and_recv('Value.MaxPopulation?'))

    def population(self):
        return int(self._send_and_recv('Value.Population?'))

    def build_point(self):
        return int(self._send_and_recv('Value.BuildPoint?'))

    def spirit_point(self):
        return int(self._send_and_recv('Value.SpiritPoint?'))

    def resources(self):
        return int(self._send_and_recv('Value.Resources?'))

    def play_speed(self):
        return int(self._send_and_recv('Value.PlaySpeed?'))

    def is_control_panel_active(self):
        return int(self._send_and_recv('Value.IsControlPanelActive?')) > 0
