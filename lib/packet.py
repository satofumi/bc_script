import connection
from datetime import ( datetime, timedelta )
import time


class Packet:
    def __init__(self):
        self._c = connection.Connection()

    def connect(self):
        self._c.open()

    def close(self):
        self._c.close()

    def send(self, message):
        #print("<< " + message)
        self._c.send(message)

    def recv(self, timeout=1.0):
        response = self._c.recv(timeout).strip()
        #print("> " + response)
        return response
