def values(actual, expected):
    assert actual == expected, 'expected: {0}, actual: {1}'.format(expected, actual)
