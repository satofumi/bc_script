import socket
import select


class Connection:
    def __init_(self):
        self._s = None

    def open(self):
        self._s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._s.connect(('127.0.0.1', 41046))

    def close(self):
        self._s.close()

    def send(self, command):
        self._s.send((command + '\n').encode())

    def recv(self, timeout = 0.1):
        ready = select.select([self._s], [], [], timeout)
        if ready[0]:
            return self._s.recv(128).decode()
        else:
            return '!timeout {0}'.format(timeout)
